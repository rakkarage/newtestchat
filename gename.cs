using System.Collections.Generic;
public class gename
{
	public class Data
	{
		public List<string> Name { get; set; }
		public List<string> Start { get; set; }
		public List<string> Middle { get; set; }
		public List<string> Finish { get; set; }
		public List<string> Pre { get; set; }
		public List<string> Post { get; set; }
	}
	public class Male : Data { }
	public class Female : Data { }
	public class Neutral : Data { }
}
